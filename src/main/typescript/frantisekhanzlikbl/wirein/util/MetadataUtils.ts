
import Path from "path"

import DirectoryMetadata from "../metadata/DirectoryMetadata"
import { letS } from "./ScopeFunctions"

export enum InclusionActionType {
	INCLUDE,
	EXCLUDE
}

export interface InclusionAction {
	type: InclusionActionType
	pattern: RegExp
}

export const buildMetadataInclusionActions = (metadata: DirectoryMetadata) => {
	return metadata.includes.map(
		(include): InclusionAction => ({
			type: InclusionActionType.INCLUDE,
			pattern: new RegExp(include)
		})
	).concat(metadata.excludes.map(
		(include): InclusionAction => ({
			type: InclusionActionType.EXCLUDE,
			pattern: new RegExp(include)
		})
	))
}

export const filterFilesByMetadata = (files: string[], metadata: DirectoryMetadata, defaultInclusionActionType: InclusionActionType) => {
	const metadataInclusionActions = buildMetadataInclusionActions(metadata)
	return files.filter(file => {
		var elementIncluded = defaultInclusionActionType == InclusionActionType.INCLUDE
		metadataInclusionActions.some(inclusionAction =>
			letS(inclusionAction.pattern.test(Path.basename(file)), isPatternMatched => {
				if (isPatternMatched) {
					elementIncluded = inclusionAction.type == InclusionActionType.INCLUDE
				}
				return isPatternMatched
			})
		)
		return elementIncluded
	})
}
