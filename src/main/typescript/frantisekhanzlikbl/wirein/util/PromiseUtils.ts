
export const forEach = async <ElementType>(array: ElementType[], block: (element: ElementType) => Promise<void>) => {
	for (const element of array) {
		await block(element)
	}
}

export const find = async <ElementType>(array: ElementType[], block: (element: ElementType) => Promise<boolean>) => {
	for (const element of array) {
		if (await block(element)) return element
	}
	return undefined
}
