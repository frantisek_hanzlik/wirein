
import Fs from "fs"

export default {
	/**
	 * Returns true if given `path` exists and is visible to this process
	 * @param path path to check
	 * @param checkForSymlink by default if `path` is symlink then this function will return if the target of the symlink exists, with this parameter set to true the existence of the symlink itself is checked instead. normal files are not affected by this parameter.
	 */
	pathExists: async (path: string, checkForSymlink = false) => {
		var exists = true
		try {
			if (checkForSymlink) await Fs.promises.lstat(path)
			else await Fs.promises.access(path, Fs.constants.F_OK)
		} catch { exists = false }
		return exists
	}
}
